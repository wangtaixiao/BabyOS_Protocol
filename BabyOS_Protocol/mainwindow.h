#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "b_protocol.h"

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);

    int ShowLog(bProtoID_t id, uint8_t cmd, uint8_t *param, bProtoLen_t param_len);
    int TransData(uint16_t num);
    int AckResult(uint8_t result);
    int AckFwInfo(void);

    ~MainWindow();

private slots:
    void on_comBUTTON_clicked();

    void on_refreshButton_clicked();

    void on_clearButton_clicked();

    void on_pushButton_clicked();

    void TimerTimeout(void);

    void on_pushButtonSetTime_clicked();

    void on_openfileBUTTON_clicked();

    void on_upgradeBUTTON_clicked();

    void on_transfileBUTTON_clicked();

    void on_stoptransfileBUTTON_clicked();

private:
    Ui::MainWindow *ui;
    QTimer         *quartTimer;
    int             protocol_n;
    QByteArray      bin_file_data;
    uint32_t        bin_file_len;
    uint32_t        bin_file_crc;
};
#endif  // MAINWINDOW_H
